package by.naakcii.api.chain;

import java.util.List;

public interface ChainService {
	
    List<ChainDTO> getAllChains();
}
