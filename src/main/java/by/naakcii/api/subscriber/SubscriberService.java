package by.naakcii.api.subscriber;

public interface SubscriberService {

    SubscriberDTO save(String email);
}
