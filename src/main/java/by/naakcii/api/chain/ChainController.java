package by.naakcii.api.chain;

import by.naakcii.api.config.ApiConfigConstants;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(description = "REST API для сущности Chain")
@RestController
@RequestMapping({"/chains"})
public class ChainController {

    private ChainService chainService;
    
    @Autowired
    public ChainController(ChainService chainService) {
    	this.chainService = chainService;
    }

    @GetMapping(produces = ApiConfigConstants.API_V_2_0)
    @ApiOperation("Возвращает список всех торговых сетей с параметром 'isActive' = true. "
    			+ "Список упорядочен по имени 'name' торговой сети.")
    public List<ChainDTO> getAllChains() {
        return chainService.getAllChains();
    }
}
