package by.naakcii.api.category;

import java.util.List;

public interface CategoryService {
    List<CategoryDTO> getAllCategories();
}
