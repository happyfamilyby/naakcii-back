package by.naakcii.api.config;

public final class ApiConfigConstants {

    public static final String API_V_2_0 = "application/vnd.naakcii.api-v2.0+json";

    private ApiConfigConstants() {
    	
    }
}
