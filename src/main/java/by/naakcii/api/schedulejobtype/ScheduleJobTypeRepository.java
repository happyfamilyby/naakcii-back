package by.naakcii.api.schedulejobtype;

import org.springframework.data.repository.CrudRepository;

public interface ScheduleJobTypeRepository extends CrudRepository<ScheduleJobType, Byte> {
}
